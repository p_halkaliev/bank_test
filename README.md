# SOLR
Solr/Lucene provides mind blowing text-analysis / stemming / full text search scoring / fuzziness functions. Things you just can not do with MySQL. In fact full text search in MySql is limited to MyIsam and scoring is very trivial and limited. Weighting fields, boosting documents on certain metrics, score results based on phrase proximity, matching accurazy etc is very hard work to almost impossible.

In Solr/Lucene you have documents. You cannot really store relations and process. Well you can of course index the keys of other documents inside a multivalued field of some document so this way you can actually store 1:n relations and do it both ways to get n:n, but its data overhead. Don't get me wrong, its perfectily fine and efficient for a lot of purposes (for example for some product catalog where you want to store the distributors for products and you want to search only parts that are available at certain distributors or something). But you reach the end of possibilities with HAS / HAS NOT. You can almonst not do something like "get all products that are available at at least 3 distributors".

Solr/Lucene has very nice facetting features and post search analysis. For example: After a very broad search that had 40000 hits you can display that you would only get 3 hits if you refined your search to the combination of having this field this value and that field that value. Stuff that need additional queries in MySQL is done efficiently and convinient.

So let's sum up

The power of Lucene is text searching/analyzing. It is also mind blowingly fast because of the reverse index structure. You can really do a lot of post processing and satisfy other needs. Altough it's document oriented and has no "graph querying" like triple stores do with SPARQL, basic N:M relations are possible to store and to query. If your application is focused on text searching you should definitely go for Solr/Lucene if you haven't good reasons, like very complex, multi-dmensional range filter queries, to do otherwise.

If you do not have text-search but rather something where you can point and click something but not enter text, good old relational databases are probably a better way to go.

You do not want to stress your database.
Get really full text search.
Perform lightning fast search results.

# build docker environment

in docker/develop folder execute

``bash
sudo  docker-compose -p test up --build -d  --force-recreate
``

# execute fixtures with purge database
``bash
php bin/console doctrine:fixtures:load
``
# execute fixtures without purge database
``bash
php bin/console doctrine:fixtures:load --append
``

# create controller
``bash
bin/console make:controller
``

# solr create core
``bash
solr create -c company_dev
``

# solr delete core
``bash
solr delete -c company_dev
``


# run php test
``bash
./vendor/bin/phpunit
``

# solr build suggester
http://localhost:8983/solr/company_dev/suggest?suggest=true&suggest.build=true&suggest.dictionary=mySuggester&suggest.q=!?