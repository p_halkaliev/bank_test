<?php

namespace App\Tests;

use App\Entity\Company;
use App\Entity\CompanyCategory;
use App\Entity\CompanyStatus;
use App\Entity\Country;
use App\Repository\CompanyCategoryRepository;
use App\Repository\CompanyStatusRepository;
use App\Repository\CountryRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use App\Service\SolrService;
class DbHelper
{
    private EntityManager $entityManager;
    private SolrService $solrService;

    public function __construct( $entityManager, $solrService )
    {
        $this->solrService = $solrService;
        $this->entityManager = $entityManager;
    }

    public function createDbRecord( string $name, string $number,string $uri, string $status, string $category, string $country ){

        $this->entityManager->getConnection()->beginTransaction();

        $companyEntity = new Company();
        $companyEntity->setName( $name );
        $companyEntity->setNumber( $number );
        $companyEntity->setUri( $uri );

        /** set status */
        /** @var CompanyStatusRepository $companyStatusRepository */
        $companyStatusRepository = $this->entityManager->getRepository( CompanyStatus::class);
        /** @var CompanyStatus $companyStatusEntity */
        $companyStatusEntity = $companyStatusRepository->getCreateByField( $status, true );
        $companyEntity->setStatus( $companyStatusEntity );


        /** set category */
        /** @var CompanyCategoryRepository $companyCategoryRepository */
        $companyCategoryRepository = $this->entityManager->getRepository( CompanyCategory::class);
        /** @var CompanyCategory $companyCategoryEntity */
        $companyCategoryEntity = $companyCategoryRepository->getCreateByField( $category, true );
        $companyEntity->setCategory( $companyCategoryEntity );


        $companyEntity->setIncorporationDate( \DateTime::createFromFormat('d/m/Y', "27/09/2019") );
        $companyEntity->setDissolutionDate( \DateTime::createFromFormat('d/m/Y', "28/09/2019") );
        $companyEntity->setConfStmtLastMadeUpdate( \DateTime::createFromFormat('d/m/Y', "30/09/2019") );
        $companyEntity->setConfStmtNextDueDate( \DateTime::createFromFormat('d/m/Y', "29/09/2019") );


        /** set country */
        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->entityManager->getRepository( Country::class);
        /** @var Country $countryEntity */
        $countryEntity = $countryRepository->getCreateByField( $country, true );
        $companyEntity->setCountryOrigin($countryEntity);

        $this->entityManager->persist( $companyEntity );
        $this->entityManager->flush();

        $this->entityManager->getConnection()->commit();

        $this->solrService->create([
            "id" => $companyEntity->getId(),
            "name" => $companyEntity->getName()
        ]);

        $this->entityManager->clear();
    }

    public function truncateTables()
    {
        $connection =  $this->entityManager->getConnection();

        $tables = $connection->getSchemaManager()->listTables();

        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tables as $table ){
            $sql = sprintf('TRUNCATE TABLE %s', $table->getName());
            $connection->executeQuery($sql);
        }
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');
    }
}