<?php

namespace App\Tests\Service;

require __DIR__."/../bootstrap.php";

use App\Service\SolrService;
use Solarium\Client;
use Solarium\QueryType\Select\Query\Query;
use Solarium\QueryType\Select\Result\Document;
use Solarium\QueryType\Select\Result\Result as SelectResult;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SolrServiceTest extends KernelTestCase
{
    private ?SolrService $solrService;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->solrService = $kernel->getContainer()
            ->get(SolrService::class);

        $this->solrService->deleteAll();
    }

    public function testClient()
    {
        $client = $this->solrService->getClient();
        $this->assertInstanceOf(Client::class, $client);
    }

    private function getRecords(): SelectResult{
        $client = $this->solrService->getClient();
        /** @var Query $query */
        $query = $client->createQuery($client::QUERY_SELECT);
        $query->setQuery("id:*");

        return $client->select( $query );
    }

    private function getRecordsNumber(): int{
        return $this->getRecords()->count();
    }

    public function testDeleteAll(){
        $this->solrService->create(["id" => 1, "name" => "test" ]);
        $this->assertEquals( 1, $this->getRecordsNumber() );

        $this->solrService->deleteAll();
        $this->assertEquals( 0, $this->getRecordsNumber() );
    }

   public function testCreate(){
       $this->solrService->create(["id" => 1, "name" => "test" ]);
       $this->assertEquals( 1, $this->getRecordsNumber() );

       /** @var Document $result */
       $result = $this->getRecords()->getIterator()->offsetGet(0);

       $this->assertEquals( "test", $result->name);
       $this->assertEquals( 1, $result->id);
   }

   public function testCreateBulk(){
       $data = [
           ["id" => 1, "name" => "test" ],
           ["id" => 2, "name" => "test2" ]
       ];
       $this->solrService->createBulk( $data );
       $this->assertEquals( 2, $this->getRecordsNumber() );
       /** @var Document $result */
       $result = $this->getRecords()->getIterator()->offsetGet(0);

       $this->assertEquals( "test", $result->name);
       $this->assertEquals( 1, $result->id);

       /** @var Document $result */
       $result = $this->getRecords()->getIterator()->offsetGet(1);

       $this->assertEquals( "test2", $result->name);
       $this->assertEquals( 2, $result->id);
   }

    protected function tearDown(): void
    {
        $this->solrService->deleteAll();
        parent::tearDown();
        $this->solrService = null;
    }
}