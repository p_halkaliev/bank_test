<?php

namespace App\Tests\Repository;

use App\Entity\Company;
use App\Entity\CompanyStatus;
use App\Repository\CompanyRepository;
use App\Repository\CompanyStatusRepository;
use App\Service\SolrService;
use App\Tests\DbHelper;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

require __DIR__."/../bootstrap.php";

class CompanyRepositoryTest extends WebTestCase
{
    private ?SolrService $solrService;
    private ?KernelBrowser $client;
    private ?EntityManager $entityManager;
    private ?dbHelper $dbHelper;

    protected function setUp(): void
    {
        self::bootKernel();
        static::ensureKernelShutdown();
        $this->client = parent::createClient();
        $this->solrService =  $this->client->getContainer()->get(SolrService::class);
        $this->entityManager = $this->client->getContainer()->get('doctrine.orm.entity_manager');

        $this->dbHelper = new DbHelper( $this->entityManager, $this->solrService);
    }

    public function testFindByIdSearch()
    {
        $this->solrService->deleteAll();
        $this->dbHelper->truncateTables();

        $this->dbHelper->createDbRecord( "digital art ltd", "111", "digital-art.com", "active", "category 2", "England" );
        $this->dbHelper->createDbRecord( "digital art ltd 2", "222", "digital-art.com", "active", "category 2", "England" );

        /** @var CompanyRepository $companyRepository */
        $companyRepository = $this->entityManager->getRepository(Company::class);
        $result = $companyRepository->findByIdSearch([1,2]);

        $this->assertIsArray( $result );
        $this->assertCount(2, $result );

        $result = $companyRepository->findByIdSearch([2]);

        $this->assertIsArray( $result );
        $this->assertCount(1, $result );

        $result = $companyRepository->findByIdSearch([0]);
        $this->assertIsArray( $result );
        $this->assertCount(0, $result );

        $result = $companyRepository->findByIdSearch(["sometext", "0", 0 ]);
        $this->assertIsArray( $result );
        $this->assertCount(0, $result );

        $this->solrService->deleteAll();
        $this->dbHelper->truncateTables();
    }

    public function testGetCreateByField(){
        /** @var CompanyStatusRepository $companyRepository */
        $companyRepository = $this->entityManager->getRepository(CompanyStatus::class);

        $this->dbHelper->truncateTables();

        $entity = $companyRepository->getCreateByField( "active", false );
        $this->assertNull( $entity );

        $entity = $companyRepository->getCreateByField( "active", true );
        $this->assertIsObject( $entity );

        $entity = $companyRepository->getCreateByField( "active", true );
        $this->assertIsObject( $entity );

        $entity = $companyRepository->getCreateByField( "active", false );
        $this->assertIsObject( $entity );

        $this->dbHelper->truncateTables();
    }

    public function testFindByName()
    {
        $this->solrService->deleteAll();
        $this->dbHelper->truncateTables();

        $this->dbHelper->createDbRecord( "digital art ltd", "111", "digital-art.com", "active", "category 2", "England" );
        $this->dbHelper->createDbRecord( "asd dsa frty", "222", "dfs.com", "active", "category 2", "England" );


        /** @var CompanyRepository $companyRepository */
        $companyRepository = $this->entityManager->getRepository(Company::class);
        $result = $companyRepository->findByName( "asd dsa", 10 );
        $this->assertIsArray( $result );
        $this->assertCount(1, $result );

        $this->assertArrayHasKey('name', $result[0]);
        $this->assertArrayHasKey('number', $result[0]);
        $this->assertArrayHasKey('address1', $result[0]);
        $this->assertArrayHasKey('address2', $result[0]);
        $this->assertArrayHasKey('postTown', $result[0]);
        $this->assertArrayHasKey('county', $result[0]);
        $this->assertArrayHasKey('country', $result[0]);
        $this->assertArrayHasKey('postcode', $result[0]);
        $this->assertEquals( "asd dsa frty", $result[0]['name']);

        $result = $companyRepository->findByName( "limited", 2 );

        $this->assertIsArray( $result );
        $this->assertCount(0, $result );

        $this->solrService->deleteAll();
        $this->dbHelper->truncateTables();
    }


    protected function tearDown(): void
    {
        parent::tearDown();
        $this->solrService = null;
        $this->dbHelper = null;
        $this->client = null;
        $this->entityManager = null;
    }
}