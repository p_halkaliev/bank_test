<?php


namespace App\Tests\Controller;
require __DIR__."/../bootstrap.php";


use App\Service\SolrService;
use App\Tests\DbHelper;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\ORM\EntityManager;

class SearchControllerTest extends WebTestCase
{
    private ?SolrService $solrService;
    private ?EntityManager $entityManager;
    private ?KernelBrowser $client;

    protected function setUp(): void
    {
        self::bootKernel();
        static::ensureKernelShutdown();
        $this->client = parent::createClient();

        $this->solrService =  $this->client->getContainer()
            ->get(SolrService::class);

        $this->entityManager = $this->client->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function testFrontend(){
        $this->solrService->getClient();
        $this->client->request('GET', '/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    private function _search($name, bool $checkForErrorMessage = false): array{
        $this->client->request('POST', '/search', ["name" => $name]);
        $response = json_decode(  $this->client->getResponse()->getContent(), true );

        if( $checkForErrorMessage ){
            $this->assertIsArray($response['message']);
            $this->assertEquals("error", $response['status']);
        }

        $this->assertEquals(200,  $this->client->getResponse()->getStatusCode());

        return $response;
    }
    public function testSearch(){

        $dbHelper = new DbHelper( $this->entityManager, $this->solrService);


        $this->_search("", true);

        $this->solrService->deleteAll();
        $dbHelper->truncateTables();


        $dbHelper->createDbRecord( "digital art ltd", "222", "digital-art.com", "active", "category 2", "England" );
        $dbHelper->createDbRecord( "new company", "1111", "new-company.com", "active", "category 2", "England" );


        $response = $this->_search("digital art ltd");

        $this->assertCount(1, $response['result']);
        $this->assertEquals('digital art ltd',  $response['result'][0]['name']);

        $response = $this->_search("digital");
        $this->assertCount(1, $response['result']);
        $this->assertEquals('digital art ltd',  $response['result'][0]['name']);

        $response = $this->_search("digi");
        $this->assertCount(1, $response['result']);
        $this->assertEquals('digital art ltd',  $response['result'][0]['name']);


        $response = $this->_search("digital");
        $this->assertCount(1, $response['result']);
        $this->assertEquals('digital art ltd',  $response['result'][0]['name']);

        $this->solrService->deleteAll();
        $dbHelper->truncateTables();
    }



    protected function tearDown(): void
    {
        $this->client = null;
        $this->solrService = null;
        $this->entityManager = null;

        parent::tearDown();

    }
}