<?php

namespace App\Repository;

use App\Entity\CompanyStatus;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyStatus[]    findAll()
 * @method CompanyStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class CompanyStatusRepository extends AbstractRepository
{
    protected bool $createRecordByName = true;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyStatus::class);
    }
}