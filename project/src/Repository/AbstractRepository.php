<?php

namespace App\Repository;

use App\Entity\CompanyStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractRepository extends ServiceEntityRepository
{
    protected bool $createRecordByName = false;

    public function __construct(ManagerRegistry $registry, $entityClass){
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param string $name
     * @param bool $createIfNotExists
     * @param string $fieldName
     * @return mixed|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function getCreateByField( string $name, bool $createIfNotExists = false, string $fieldName = "name" ){

        if( !$this->createRecordByName ){
            throw new \Exception( "Entity ".$this->_entityName." cannot be created only by name" );
        }

        $entity = $this->findOneBy([ $fieldName => $name ]);

        if( $entity == null and $createIfNotExists == true ){

            $entity = new $this->_entityName();
            $entity->setName( $name );

            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();
        }

        return $entity;
    }
}