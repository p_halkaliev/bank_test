<?php

namespace App\Repository;

use App\Entity\CompanyCategory;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyCategory[]    findAll()
 * @method CompanyCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class CompanyCategoryRepository extends AbstractRepository
{
    protected bool $createRecordByName = true;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyCategory::class);
    }
}