<?php

namespace App\Repository;

use App\Entity\AccountCategory;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountCategory[]    findAll()
 * @method AccountCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class AccountCategoryRepository extends AbstractRepository
{
    protected bool $createRecordByName = true;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountCategory::class);
    }
}