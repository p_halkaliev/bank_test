<?php

namespace App\Repository;

use App\Entity\Company;
use App\Service\SolrService;
use Doctrine\Persistence\ManagerRegistry;
use Solarium\QueryType\Select\Query\Query;
use Solarium\QueryType\Select\Result\Document;
use Solarium\QueryType\Select\Result\Result as SelectResult;
use Solarium\QueryType\Suggester\Result\Dictionary;
use Solarium\QueryType\Suggester\Result\Result as SuggesterResult;
use Solarium\QueryType\Suggester\Result\Term;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class CompanyRepository extends AbstractRepository
{
    private SolrService $solr;

    public function __construct(SolrService $solr, ManagerRegistry $registry)
    {
        $this->solr = $solr;
        parent::__construct($registry, Company::class);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function findByIdSearch( array $ids ): array {

        $result = $this->getEntityManager()
                       ->createQueryBuilder()
                       ->select(["c.id",'c.name', 'c.number', "a.address1","a.address2","a.postTown","a.county", "a.country", "a.postcode"])
                       ->from("App\Entity\Company", "c")
                       ->leftJoin("c.regAddress", "a")
                       ->where('c.id IN (:ids)')
                       ->setParameter('ids', $ids)
                       ->getQuery()
                       ->getArrayResult();

        return $result;
    }


    /**
     * @param string $name
     * @param int $maxRows
     * @param string $field
     * @return SelectResult
     */
    private function match( string $name, int $maxRows, string $field, bool $toLowerCase = false, array $previousResult = []): SelectResult{
        $client = $this->solr->getClient();

        /** @var Query $query */
        $query = $client->createQuery($client::QUERY_SELECT);
        $query->setFields(["id", "name"]);

        $name = $toLowerCase ? strtolower( $name ) : $name;

        $name = $query->getHelper()->escapePhrase( $name );

        $suffix = "";
        if( !empty( $previousResult ) ){
            foreach ( $previousResult as $row ){
                $suffix .=" NOT id:".$row['id'];
            }
        }

        $queryPattern = $field.":".$name.$suffix;
        $query->setQuery($queryPattern);
        $query->setStart(0);
        $query->setRows($maxRows);

        return $client->select( $query );
    }

    private function _getResult( object $result, array $resultDb ): array {
        $ids = [];
        /** @var Document $doc */
        foreach ( $result->getDocuments() as $doc ){
            $ids[] = $doc->getFields()['id'];
        }
        $resultSet = $this->findByIdSearch( $ids );

        return array_merge($resultDb, $resultSet);
    }
    /**
     * @param string $name
     * @param int $maxRows
     * @return array
     */
    public function findByName( string $name, int $maxRows = 10 ): array {

        $result = $this->match( $name, $maxRows, "cname", true );

        $resultDb = [];

        if( $result->count() ){
            $resultDb = $this->_getResult( $result, $resultDb );
        }


        if( !count( $resultDb )) {
            $result = $this->match($name, $maxRows-count( $resultDb ), "name", true, $resultDb);
            if( $result->count() ){
                $resultDb = $this->_getResult( $result, $resultDb );
            }
        }

        return $resultDb;
    }
}