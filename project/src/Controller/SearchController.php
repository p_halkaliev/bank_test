<?php

namespace App\Controller;

use App\Entity\Company;
use App\Repository\CompanyRepository;
use App\Service\SolrService;
use App\Validator\SearchValidation;
use Solarium\QueryType\Select\Result\Document;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/",methods={"GET"}, name="search")
     * @return Response
     */
    public function index( ): Response
    {
        return $this->render('search.html.twig');
    }

    /**
     * @Route("/search",methods={"POST"}, name="search-request")
     * @param SearchValidation $validator
     * @param Request $request
     * @return JsonResponse
     */
    public function search(SearchValidation $validator,  Request $request )
    {
        header(' 500 Internal Server Error', true, 500);
        exit;
        $violationList = $validator->Validate( $request->request->all() );

        if( $violationList != null ){
            return $this->json( $violationList );
        }

        $name = $request->get("name");

        /** @var CompanyRepository $repository */
        $companyRepository = $this->getDoctrine()->getRepository( Company::class );

        $result = $companyRepository->findByName( $name );


        return $this->json(
            [
                'status' => 'ok',
                "result" => $result
            ]
        );
    }
}
