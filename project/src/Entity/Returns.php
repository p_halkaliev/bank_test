<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Return
 *
 * @ORM\Table(name="returns", indexes={@ORM\Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class Returns
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_due_date", type="date", nullable=true)
     */
    private ?\DateTime $nextDueDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_made_update", type="date", nullable=true)
     */
    private ?\DateTime $lastMadeUpdate;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private Company $company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ?\DateTime
     */
    public function getNextDueDate(): ?\DateTime
    {
        return $this->nextDueDate;
    }

    /**
     * @param ?\DateTime $nextDueDate
     */
    public function setNextDueDate(?\DateTime $nextDueDate): void
    {
        $this->nextDueDate = $nextDueDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastMadeUpdate(): ?\DateTime
    {
        return $this->lastMadeUpdate;
    }

    /**
     * @param \DateTime|null $lastMadeUpdate
     */
    public function setLastMadeUpdate(?\DateTime $lastMadeUpdate): void
    {
        $this->lastMadeUpdate = $lastMadeUpdate;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }


}
