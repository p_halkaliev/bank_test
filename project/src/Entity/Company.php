<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})}, indexes={@ORM\Index(name="FK_company_company_status", columns={"status_id"}), @ORM\Index(name="category_id", columns={"category_id"}), @ORM\Index(name="FK_company_country", columns={"country_origin_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private string $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="number", type="string", length=50, nullable=true)
     */
    private ?string $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dissolution_date", type="date", nullable=true)
     */
    private \DateTime $dissolutionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="incorporation_date", type="date", nullable=true)
     */
    private \DateTime $incorporationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="conf_stmt_next_due_date", type="date", nullable=true)
     */
    private \DateTime $confStmtNextDueDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="conf_stmt_last_made_update", type="date", nullable=true)
     */
    private ?\DateTime $confStmtLastMadeUpdate;

    /**
     * @var CompanyCategory
     *
     * @ORM\ManyToOne(targetEntity="CompanyCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private CompanyCategory $category;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uri", type="text", length=16777215, nullable=true)
     */
    private ?string $uri;

    /**
     * @var CompanyStatus
     *
     * @ORM\ManyToOne(targetEntity="CompanyStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * })
     */
    private CompanyStatus $status;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_origin_id", referencedColumnName="id")
     * })
     */
    private Country $countryOrigin;


    /**
     * @var ?ArrayCollection
     * @ORM\OneToMany(targetEntity="RegAddress", mappedBy="company")
     */
    private $regAddress;


    public function __construct() {
        $this->regAddress = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }

    /**
     * @param string|null $uri
     */
    public function setUri(?string $uri): void
    {
        $this->uri = $uri;
    }
    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string|null $number
     */
    public function setNumber(?string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return \DateTime
     */
    public function getDissolutionDate(): \DateTime
    {
        return $this->dissolutionDate;
    }

    /**
     * @param \DateTime $dissolutionDate
     */
    public function setDissolutionDate(\DateTime $dissolutionDate): void
    {
        $this->dissolutionDate = $dissolutionDate;
    }

    /**
     * @return \DateTime
     */
    public function getIncorporationDate(): \DateTime
    {
        return $this->incorporationDate;
    }

    /**
     * @param \DateTime $incorporationDate
     */
    public function setIncorporationDate(\DateTime $incorporationDate): void
    {
        $this->incorporationDate = $incorporationDate;
    }

    /**
     * @return \DateTime
     */
    public function getConfStmtNextDueDate(): \DateTime
    {
        return $this->confStmtNextDueDate;
    }

    /**
     * @param \DateTime $confStmtNextDueDate
     */
    public function setConfStmtNextDueDate(\DateTime $confStmtNextDueDate): void
    {
        $this->confStmtNextDueDate = $confStmtNextDueDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getConfStmtLastMadeUpdate(): ?\DateTime
    {
        return $this->confStmtLastMadeUpdate;
    }

    /**
     * @param \DateTime|null $confStmtLastMadeUpdate
     */
    public function setConfStmtLastMadeUpdate(?\DateTime $confStmtLastMadeUpdate): void
    {
        $this->confStmtLastMadeUpdate = $confStmtLastMadeUpdate;
    }

    /**
     * @return CompanyCategory
     */
    public function getCategory(): CompanyCategory
    {
        return $this->category;
    }

    /**
     * @param CompanyCategory $category
     */
    public function setCategory(CompanyCategory $category): void
    {
        $this->category = $category;
    }

    /**
     * @return CompanyStatus
     */
    public function getStatus(): CompanyStatus
    {
        return $this->status;
    }

    /**
     * @param CompanyStatus $status
     */
    public function setStatus(CompanyStatus $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Country
     */
    public function getCountryOrigin(): Country
    {
        return $this->countryOrigin;
    }

    /**
     * @param Country $countryOrigin
     */
    public function setCountryOrigin(Country $countryOrigin): void
    {
        $this->countryOrigin = $countryOrigin;
    }

    /**
     * @return ArrayCollection
     */
    public function getRegAddress(): ArrayCollection
    {
        return $this->regAddress;
    }

    /**
     * @param RegAddress $regAddress
     */
    public function setRegAddress(RegAddress $regAddress): void
    {
        $this->regAddress->add( $regAddress );
    }
}
