<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Morgages
 *
 * @ORM\Table(name="mortgages", indexes={@ORM\Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class Mortgages
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="num_mort_charges", type="integer", nullable=true)
     */
    private ?int $numMortCharges;

    /**
     * @var int|null
     *
     * @ORM\Column(name="num_mort_outstanding", type="integer", nullable=true)
     */
    private ?int $numMortOutstanding;

    /**
     * @var int|null
     *
     * @ORM\Column(name="num_mort_part_satisfied", type="integer", nullable=true)
     */
    private ?int $numMortPartSatisfied;

    /**
     * @var int|null
     *
     * @ORM\Column(name="num_mort_satisfied", type="integer", nullable=true)
     */
    private ?int $numMortSatisfied;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private Company $company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getNumMortCharges(): ?int
    {
        return $this->numMortCharges;
    }

    /**
     * @param int|null $numMortCharges
     */
    public function setNumMortCharges(?int $numMortCharges): void
    {
        $this->numMortCharges = $numMortCharges;
    }

    /**
     * @return int|null
     */
    public function getNumMortOutstanding(): ?int
    {
        return $this->numMortOutstanding;
    }

    /**
     * @param int|null $numMortOutstanding
     */
    public function setNumMortOutstanding(?int $numMortOutstanding): void
    {
        $this->numMortOutstanding = $numMortOutstanding;
    }

    /**
     * @return int|null
     */
    public function getNumMortPartSatisfied(): ?int
    {
        return $this->numMortPartSatisfied;
    }

    /**
     * @param int|null $numMortPartSatisfied
     */
    public function setNumMortPartSatisfied(?int $numMortPartSatisfied): void
    {
        $this->numMortPartSatisfied = $numMortPartSatisfied;
    }

    /**
     * @return int|null
     */
    public function getNumMortSatisfied(): ?int
    {
        return $this->numMortSatisfied;
    }

    /**
     * @param int|null $numMortSatisfied
     */
    public function setNumMortSatisfied(?int $numMortSatisfied): void
    {
        $this->numMortSatisfied = $numMortSatisfied;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }
}
