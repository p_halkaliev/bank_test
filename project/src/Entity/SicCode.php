<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SicCode
 *
 * @ORM\Table(name="sic_code", indexes={@ORM\Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class SicCode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sic_text_1", type="text", length=16777215, nullable=true)
     */
    private ?string $sicText1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sic_text_2", type="text", length=16777215, nullable=true)
     */
    private ?string $sicText2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sic_text_3", type="text", length=16777215, nullable=true)
     */
    private ?string $sicText3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sic_text_4", type="text", length=16777215, nullable=true)
     */
    private ?string $sicText4;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private Company $company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSicText1(): ?string
    {
        return $this->sicText1;
    }

    /**
     * @param string|null $sicText1
     */
    public function setSicText1(?string $sicText1): void
    {
        $this->sicText1 = $sicText1;
    }

    /**
     * @return string|null
     */
    public function getSicText2(): ?string
    {
        return $this->sicText2;
    }

    /**
     * @param string|null $sicText2
     */
    public function setSicText2(?string $sicText2): void
    {
        $this->sicText2 = $sicText2;
    }

    /**
     * @return string|null
     */
    public function getSicText3(): ?string
    {
        return $this->sicText3;
    }

    /**
     * @param string|null $sicText3
     */
    public function setSicText3(?string $sicText3): void
    {
        $this->sicText3 = $sicText3;
    }

    /**
     * @return string|null
     */
    public function getSicText4(): ?string
    {
        return $this->sicText4;
    }

    /**
     * @param string|null $sicText4
     */
    public function setSicText4(?string $sicText4): void
    {
        $this->sicText4 = $sicText4;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }


}
