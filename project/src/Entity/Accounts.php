<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accounts
 *
 * @ORM\Table(name="accounts", indexes={@ORM\Index(name="category_id", columns={"category_id"}), @ORM\Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class Accounts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ref_day", type="integer", nullable=true)
     */
    private ?int $refDay;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ref_month", type="integer", nullable=true)
     */
    private ?int $refMonth;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="next_due_date", type="date", nullable=true)
     */
    private ?\DateTime $nextDueDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_made_update", type="date", nullable=true)
     */
    private ?\DateTime $lastMadeUpdate;

    /**
     * @var AccountCategory
     *
     * @ORM\ManyToOne(targetEntity="AccountCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private AccountCategory $category;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private Company $company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getRefDay(): ?int
    {
        return $this->refDay;
    }

    /**
     * @param int|null $refDay
     */
    public function setRefDay(?int $refDay): void
    {
        $this->refDay = $refDay;
    }

    /**
     * @return int|null
     */
    public function getRefMonth(): ?int
    {
        return $this->refMonth;
    }

    /**
     * @param int|null $refMonth
     */
    public function setRefMonth(?int $refMonth): void
    {
        $this->refMonth = $refMonth;
    }

    /**
     * @return \DateTime|null
     */
    public function getNextDueDate(): ?\DateTime
    {
        return $this->nextDueDate;
    }

    /**
     * @param \DateTime|null $nextDueDate
     */
    public function setNextDueDate(?\DateTime $nextDueDate): void
    {
        $this->nextDueDate = $nextDueDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastMadeUpdate(): ?\DateTime
    {
        return $this->lastMadeUpdate;
    }

    /**
     * @param \DateTime|null $lastMadeUpdate
     */
    public function setLastMadeUpdate(?\DateTime $lastMadeUpdate): void
    {
        $this->lastMadeUpdate = $lastMadeUpdate;
    }

    /**
     * @return AccountCategory
     */
    public function getCategory(): AccountCategory
    {
        return $this->category;
    }

    /**
     * @param AccountCategory $category
     */
    public function setCategory(AccountCategory $category): void
    {
        $this->category = $category;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }


}
