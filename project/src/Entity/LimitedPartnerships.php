<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LimitedPartnerships
 *
 * @ORM\Table(name="limited_partnerships", indexes={@ORM\Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class LimitedPartnerships
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var int
     *
     * @ORM\Column(name="num_gen_partners", type="integer", nullable=false)
     */
    private int $numGenPartners;

    /**
     * @var ?int
     *
     * @ORM\Column(name="num_lim_partners", type="integer", nullable=true)
     */
    private int $numLimPartners;



    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private Company $company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumGenPartners(): int
    {
        return $this->numGenPartners;
    }

    /**
     * @param int $numGenPartners
     */
    public function setNumGenPartners(int $numGenPartners): void
    {
        $this->numGenPartners = $numGenPartners;
    }

    /**
     * @return ?int
     */
    public function getNumLimPartners(): ?int
    {
        return $this->numLimPartners;
    }

    /**
     * @param ?int $numLimPartners
     */
    public function setNumLimPartners(?int $numLimPartners): void
    {
        $this->numLimPartners = $numLimPartners;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }


}
