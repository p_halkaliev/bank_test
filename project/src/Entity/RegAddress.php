<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RegAddress
 *
 * @ORM\Table(name="reg_address", indexes={@ORM\Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class RegAddress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="care_of", type="string", length=200, nullable=false)
     */
    private string $careOf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pobox", type="string", length=50, nullable=true)
     */
    private ?string $pobox;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address1", type="string", length=50, nullable=true)
     */
    private ?string $address1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address2", type="string", length=50, nullable=true)
     */
    private ?string $address2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post_town", type="string", length=50, nullable=true)
     */
    private ?string $postTown;

    /**
     * @var string|null
     *
     * @ORM\Column(name="county", type="string", length=50, nullable=true)
     */
    private ?string $county;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="string", length=50, nullable=true)
     */
    private ?string $country;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postcode", type="string", nullable=true)
     */
    private ?string $postcode;


    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="reg_address")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private Company $company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCareOf(): string
    {
        return $this->careOf;
    }

    /**
     * @param string $careOf
     */
    public function setCareOf(string $careOf): void
    {
        $this->careOf = $careOf;
    }

    /**
     * @return string|null
     */
    public function getPobox(): ?string
    {
        return $this->pobox;
    }

    /**
     * @param string|null $pobox
     */
    public function setPobox(?string $pobox): void
    {
        $this->pobox = $pobox;
    }

    /**
     * @return string|null
     */
    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    /**
     * @param string|null $address1
     */
    public function setAddress1(?string $address1): void
    {
        $this->address1 = $address1;
    }

    /**
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     */
    public function setAddress2(?string $address2): void
    {
        $this->address2 = $address2;
    }

    /**
     * @return string|null
     */
    public function getPostTown(): ?string
    {
        return $this->postTown;
    }

    /**
     * @param string|null $postTown
     */
    public function setPostTown(?string $postTown): void
    {
        $this->postTown = $postTown;
    }

    /**
     * @return string|null
     */
    public function getCounty(): ?string
    {
        return $this->county;
    }

    /**
     * @param string|null $county
     */
    public function setCounty(?string $county): void
    {
        $this->county = $county;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * @param string|null $postcode
     */
    public function setPostcode(?string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }






}
