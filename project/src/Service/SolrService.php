<?php
namespace App\Service;

use Solarium\Core\Client\Adapter\Curl;
use Solarium\QueryType\Update\Result as UpdateResult;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Solarium\Client;

class SolrService
{
    private Client $client;
    private string $dsn;

    public function __construct( ParameterBagInterface $params){

        $this->dsn = $params->get('SOLR_DSN');

        $eventDispatcher = new EventDispatcher();
        $adapter = new Curl();
        $config = array(
            'endpoint' => array(
                'localhost' => array(
                    'host' => $params->get('SOLR_HOST'),
                    'port' => $params->get('SOLR_PORT'),
                    'path' => $params->get('SOLR_PATH'),
                    'core' => $params->get('SOLR_CORE')
                )
            )
        );

        $this->client = new Client($adapter, $eventDispatcher, $config);
    }

    public function getClient(): Client{
        return $this->client;
    }

    /**
     * @return UpdateResult
     */
    public function deleteAll(): UpdateResult {
        $update = $this->client->createUpdate();

        $update->addDeleteQuery('*:*');
        $update->addCommit();
        return $this->client->update($update);
    }

    public function escapeSolrValue($string)
    {
        $match = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ');
        $replace = array('\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;', '\\ ');
        $string = str_replace($match, $replace, $string);

        return $string;
    }

    public function createBulk( array $data ): UpdateResult {
        $update = $this->client->createUpdate();

        $dataForInsert = [];

        foreach ( $data as $record ){
            $doc = $update->createDocument();
            foreach ( $record as $key => $val){
                if( $key == "name" ){
                    $doc->cname = strtolower( $val );
                }
                $doc->$key = $val;
            }
            $dataForInsert[] = $doc;
        }

        $update->addDocuments($dataForInsert);
        $update->addCommit();

        return $this->client->update($update);
    }
    /**
     * @param array $data
     * @return UpdateResult
     */
    public function create( array $data ): UpdateResult {
        $update = $this->client->createUpdate();

        $doc = $update->createDocument();

        foreach ( $data as $key => $val){
            if( $key == "name" ){
                $doc->cname = strtolower( $val );
            }
            $doc->$key = $val;
        }

        $update->addDocuments([$doc]);
        $update->addCommit();
        return $this->client->update($update);
    }
}