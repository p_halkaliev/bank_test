<?php

namespace App\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

class SearchValidation
{
    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function Validate( array $input ): ?array{

        $validator = Validation::createValidator();

        $constraints = new Collection([
            'name' => [
                new Length([
                    "min" => 2,
                    "minMessage" => "Name cannot be less than {{ limit }} characters",
                ])
            ]
        ]);

        /** @var ConstraintViolationList $violations */
        $violations = $validator->validate($input, $constraints );

        return $this->getErrorMessage( $violations );
    }

    protected function getErrorMessage( ConstraintViolationList $violationList ): ?array{
        if( $violationList->count() ){
            $messages = [];
            /** @var ConstraintViolation $validator */
            foreach( $violationList->getIterator() as $validator ){
                $messages[] = $validator->getMessage();
            }

            return [
                'status' => 'error',
                'message' => $messages
            ];
        }

        return null;
    }
}