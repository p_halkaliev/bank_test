<?php

namespace App\DataFixtures;

use App\Entity\AccountCategory;
use App\Entity\Accounts;
use App\Entity\Company;
use App\Entity\CompanyCategory;
use App\Entity\CompanyPreviousName;
use App\Entity\CompanyStatus;
use App\Entity\Country;
use App\Entity\LimitedPartnerships;
use App\Entity\Mortgages;
use App\Entity\RegAddress;
use App\Entity\Returns;
use App\Entity\SicCode;
use App\Repository\AccountCategoryRepository;
use App\Repository\CompanyCategoryRepository;
use App\Repository\CompanyRepository;
use App\Repository\CompanyStatusRepository;
use App\Repository\CountryRepository;
use App\Service\SolrService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;


class SolrFixtures extends Fixture implements DependentFixtureInterface
{
    private int $itemsPerPage = 50000;
    private SolrService $solr;

    public function __construct(SolrService $solr)
    {
        $this->solr = $solr;
    }

    public function load(ObjectManager $manager)
    {

        echo "Creating Solr index\n";
        $this->solr->deleteAll();

        $i = 0;
        while(true){
           $result = $manager
               ->createQueryBuilder()
               ->select(['c.name', 'c.id'])
               ->from("App\Entity\Company", "c")
               ->addOrderBy('c.id', 'ASC')
               ->setFirstResult($i*$this->itemsPerPage)
               ->setMaxResults($this->itemsPerPage)
               ->getQuery()
               ->getArrayResult();

            $this->solr->createBulk( $result );
            echo ($i+1)*$this->itemsPerPage." records indexed\n";
            $i++;

            if( empty( $result ) ){
               break;
            }
         }

        echo "Solr index created\n";
    }

    public function getDependencies()
    {
        return array(
            DatabaseFixtures::class,
        );
    }
}
