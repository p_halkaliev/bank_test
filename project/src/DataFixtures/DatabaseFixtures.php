<?php

namespace App\DataFixtures;

use App\Entity\AccountCategory;
use App\Entity\Accounts;
use App\Entity\Company;
use App\Entity\CompanyCategory;
use App\Entity\CompanyPreviousName;
use App\Entity\CompanyStatus;
use App\Entity\Country;
use App\Entity\LimitedPartnerships;
use App\Entity\Mortgages;
use App\Entity\RegAddress;
use App\Entity\Returns;
use App\Entity\SicCode;
use App\Repository\AccountCategoryRepository;
use App\Repository\CompanyCategoryRepository;
use App\Repository\CompanyRepository;
use App\Repository\CompanyStatusRepository;
use App\Repository\CountryRepository;
use App\Service\SolrService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;


class DatabaseFixtures extends Fixture
{
    private Connection $connection;
    private ObjectManager $manager;
    private int $lineNumber = 0;
    private string $filePath = __DIR__."/files/BasicCompanyDataAsOneFile-2020-11-01.csv";

    /**
     * @throws ORMException
     */
    private function readCSV(){
        $handle = fopen( $this->filePath, "r" );

        while (($raw_string = fgets($handle)) !== false) {
            $this->lineNumber++;

            // skip column names
            if( $this->lineNumber == 1){
                continue;
            }

            if( $this->lineNumber < 2500001){
                continue;
            }

            /** @var array $row */
            $row = str_getcsv($raw_string);

            $this->writeToDatabase( $row );
        }

        fclose($handle);

    }

    /**
     * @throws Exception
     */
    private function truncateTables(){

        $tables = $this->connection->fetchAllNumeric('SHOW TABLES');

        $dbPlatform = $this->connection->getDatabasePlatform();
        $this->connection->executeQuery('SET FOREIGN_KEY_CHECKS=0');

        foreach ($tables as $table ){
            $q = $dbPlatform->getTruncateTableSql($table[0]);
            $this->connection->executeStatement($q);
        }

        $this->connection->executeQuery('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * @param ObjectManager $manager
     * @throws Exception
     * @throws ORMException
     */
    public function load(ObjectManager $manager)
    {
        return;
        $this->manager = $manager;
        $this->connection = $this->manager->getConnection();

        // disable sql logging
        // HUGE MEMORY LEAK IF NOT DISABLED ( memory consumption will rise every iteration )
        $this->connection->getConfiguration()->setSQLLogger(null);
        echo "\n\n Start importing...";

        // truncate database tables
        // $this->truncateTables();

        $this->readCSV();


        echo "\nImport finished\n";
    }

    /**
     * @param array $row
     * @throws ORMException
     */
    private function writeToDatabase( array $row ){

        try {
            $this->connection->beginTransaction();
            $companyEntity = $this->writeCompany($row);

            if( $companyEntity != null ) {
                $this->writeRegAddress($companyEntity, $row);
                /*$this->writeAccounts($companyEntity, $row);
                $this->writeMortgages($companyEntity, $row);
                $this->writePreviousNames($companyEntity, $row);
                $this->writeLimitedPartnership($companyEntity, $row);
                $this->writeReturns($companyEntity, $row);
                $this->writeSICcode($companyEntity, $row);*/
            }


            $this->manager->flush();
            $this->manager->clear();


            $memory = "Memory: ".((memory_get_usage(true) / 1024) / 1024) . "mb \n";
            echo "\nLine {$this->lineNumber} imported ".$memory;
            $this->connection->commit();
        } catch (ConnectionException $e) {
            try {
                $this->connection->rollBack();
            } catch (ConnectionException $ex) {
                echo $ex->getMessage()."\n"; exit;
            }
            echo $e->getMessage()."\n"; exit;
        }

    }

    /**
     * @param Company $companyEntity
     * @param array $row
     */
    private function writeSICcode( Company $companyEntity, array $row ): void{

        $sicCodeEntity = new SicCode();
        $sicCodeEntity->setCompany( $companyEntity );
        $sicCodeEntity->setSicText1($row[DataField::$sicCodeText1]);
        $sicCodeEntity->setSicText2($row[DataField::$sicCodeText2]);
        $sicCodeEntity->setSicText3($row[DataField::$sicCodeText3]);
        $sicCodeEntity->setSicText4($row[DataField::$sicCodeText4]);

        $this->manager->persist( $sicCodeEntity );
    }

    private function writeReturns( Company $companyEntity, array $row ): void{
        $returnEntity = new Returns();
        $returnEntity->setCompany( $companyEntity );

        $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$returnsLastMadeUpdate]);
        if( $date ){
            $returnEntity->setLastMadeUpdate( $date );
        }

        $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$returnsNumDueDate]);
        if( $date ){
            $returnEntity->setNextDueDate( $date );
        }

        $this->manager->persist( $returnEntity );
    }

    private function writeLimitedPartnership( Company $companyEntity, array $row ): void{

        $limitedPartnershipsEntity = new LimitedPartnerships();
        $limitedPartnershipsEntity->setCompany( $companyEntity );

        if( is_integer( $row[DataField::$limitedPartnershipsNumGenPartners] )){
            $limitedPartnershipsEntity->setNumGenPartners( $row[DataField::$limitedPartnershipsNumGenPartners] );
        }

        if( is_integer( $row[DataField::$limitedPartnershipsNumLimPartners] )){
            $limitedPartnershipsEntity->setNumLimPartners( $row[DataField::$limitedPartnershipsNumLimPartners] );
        }

        $this->manager->persist( $limitedPartnershipsEntity );
    }

    /**
     * @param Company $companyEntity
     * @param array $row
     */
    private function writePreviousNames( Company $companyEntity, array $row ): void{

        $data = [
            [
                "condate" => $row[DataField::$companyPreviousName_Condate1],
                "name" => $row[DataField::$companyPreviousName_Name1]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate2],
                "name" => $row[DataField::$companyPreviousName_Name2]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate3],
                "name" => $row[DataField::$companyPreviousName_Name3]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate4],
                "name" => $row[DataField::$companyPreviousName_Name4]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate5],
                "name" => $row[DataField::$companyPreviousName_Name5]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate6],
                "name" => $row[DataField::$companyPreviousName_Name6]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate7],
                "name" => $row[DataField::$companyPreviousName_Name7]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate8],
                "name" => $row[DataField::$companyPreviousName_Name8]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate9],
                "name" => $row[DataField::$companyPreviousName_Name9]
            ],
            [
                "condate" => $row[DataField::$companyPreviousName_Condate10],
                "name" => $row[DataField::$companyPreviousName_Name10]
            ]
        ];

        foreach( $data as $val ){
            if( !empty( $val["name"] )){
                $companyPreviousNameEntity = new CompanyPreviousName();
                $companyPreviousNameEntity->setCompany( $companyEntity );
                $companyPreviousNameEntity->setName( $val["name"] );

                $date = \DateTime::createFromFormat('d/m/Y', $val["condate"]);
                if( $date ){
                    $companyPreviousNameEntity->setCondate( $date );
                }

                $this->manager->persist( $companyPreviousNameEntity );
            }
        }
    }

    /**
     * @param Company $companyEntity
     * @param array $row
     */
    private function writeMortgages( Company $companyEntity, array $row ): void{

        $mortgageEntity = new Mortgages();
        $mortgageEntity->setCompany( $companyEntity );
        $mortgageEntity->setNumMortCharges( $row[DataField::$mortgagesNumMortCharges] );
        $mortgageEntity->setNumMortOutstanding( $row[DataField::$mortgagesNumMortOutstanding] );

        if( is_integer( $row[DataField::$mortgagesNumMortPartSatisfied] )){
            $mortgageEntity->setNumMortPartSatisfied( $row[DataField::$mortgagesNumMortPartSatisfied] );
        }

        if( is_integer( $row[DataField::$mortgagesNumMortSatisfied] )){
            $mortgageEntity->setNumMortSatisfied( $row[DataField::$mortgagesNumMortSatisfied] );
        }

        $this->manager->persist( $mortgageEntity );
    }

    /**
     * @param Company $companyEntity
     * @param array $row
     */
    private function writeAccounts( Company $companyEntity, array $row ): void{

        $accountEntity = new Accounts();
        $accountEntity->setCompany( $companyEntity );


        /** @var AccountCategoryRepository $companyStatusRepository */
        $accountCategoryRepository = $this->manager->getRepository( AccountCategory::class);
        /** @var AccountCategory $companyStatusEntity */
        $accountCategoryEntity = $accountCategoryRepository->getCreateByField( $row[DataField::$accountCategoryCategory], true );
        $accountEntity->setCategory( $accountCategoryEntity );


        $accountEntity->setRefDay( (int)$row[DataField::$accountCategoryRefDay] );
        $accountEntity->setRefMonth( (int)$row[DataField::$accountCategoryRefMonth] );


        $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$accountCategoryNextDueDate]);
        if( $date ){
            $accountEntity->setNextDueDate( $date );
        }


        $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$accountCategoryLastMadeUpdate]);
        if( $date ){
            $accountEntity->setLastMadeUpdate( $date );
        }

        $this->manager->persist( $accountEntity );

    }

    /**
     * @param Company $companyEntity
     * @param array $row
     */
    private function writeRegAddress( Company $companyEntity, array $row ): void{

        $regAddressEntity = new RegAddress();
        $regAddressEntity->setCompany( $companyEntity );
        $regAddressEntity->setAddress1( $row[DataField::$regAddressLineText1] );
        $regAddressEntity->setAddress2( $row[DataField::$regAddressLineText2] );
        $regAddressEntity->setCareOf( $row[DataField::$regAddressCareOf] );
        $regAddressEntity->setCountry( $row[DataField::$regAddressCountry] );
        $regAddressEntity->setCounty( $row[DataField::$regAddressCounty] );
        $regAddressEntity->setPobox( $row[DataField::$regAddressPobox] );
        $regAddressEntity->setPostcode( $row[DataField::$regAddressPostcode] );
        $regAddressEntity->setPostTown( $row[DataField::$regAddressPostTown] );

        $this->manager->persist( $regAddressEntity );
    }
    /**
     * @param array $row
     * @return Company
     * @throws ORMException
     */
    private function writeCompany( array $row ): ?Company {

        /** @var CompanyRepository  $companyRepository */
        $companyRepository = $this->manager->getRepository( Company::class);
        /** @var Company|null $companyEntity */
        $companyEntity = $companyRepository->findOneBy(["name" => $row[DataField::$companyName] ]);

        if( $companyEntity != null ){
            return null;
        }

        $companyEntity = new Company();
        $companyEntity->setName( $row[DataField::$companyName] );
        $companyEntity->setNumber( $row[DataField::$companyNumber] );
        $companyEntity->setUri( $row[DataField::$companyURI] );

        /** set status */
        /** @var CompanyStatusRepository $companyStatusRepository */
        $companyStatusRepository = $this->manager->getRepository( CompanyStatus::class);
        /** @var CompanyStatus $companyStatusEntity */
        $companyStatusEntity = $companyStatusRepository->getCreateByField( $row[DataField::$companyStatus], true );
        $companyEntity->setStatus( $companyStatusEntity );


        /** set category */
        /** @var CompanyCategoryRepository $companyCategoryRepository */
        $companyCategoryRepository = $this->manager->getRepository( CompanyCategory::class);
        /** @var CompanyCategory $companyCategoryEntity */
        $companyCategoryEntity = $companyCategoryRepository->getCreateByField( $row[DataField::$companyCategory], true );
        $companyEntity->setCategory( $companyCategoryEntity );

        if( !empty( $row[DataField::$incorporationDate] )) {
            $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$incorporationDate]);
            if ($date) {
                $companyEntity->setIncorporationDate($date);
            }
        }

        if( !empty( $row[DataField::$dissolutionDate] )) {
            $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$dissolutionDate]);
            if ($date) {
                $companyEntity->setDissolutionDate($date);
            }
        }

        if( !empty( $row[DataField::$confStmtLastMadeUpdate] )){
            $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$confStmtLastMadeUpdate]);
            if( $date ){
                $companyEntity->setConfStmtLastMadeUpdate( $date );
            }
        }

        if( !empty( $row[DataField::$confStmtNextDueDate] )) {
            $date = \DateTime::createFromFormat('d/m/Y', $row[DataField::$confStmtNextDueDate]);
            if ($date) {
                $companyEntity->setConfStmtNextDueDate($date);
            }
        }

        /** set country */
        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->manager->getRepository( Country::class);
        /** @var Country $countryEntity */
        $countryEntity = $countryRepository->getCreateByField( $row[DataField::$countryOrigin], true );
        $companyEntity->setCountryOrigin($countryEntity);

        $this->manager->persist( $companyEntity );
        $this->manager->flush();

        return $companyEntity;
    }
}
