<?php


namespace App\DataFixtures;


abstract class DataField
{
    /** company table  */
    public static int $companyNumber = 1;
    public static int $confStmtNextDueDate = 53;
    public static int $confStmtLastMadeUpdate = 54;
    public static int $dissolutionDate = 13;
    public static int $incorporationDate = 14;
    public static int $countryOrigin = 12;
    public static int $companyName = 0;
    public static int $companyStatus = 11;
    public static int $companyCategory = 10;
    public static int $companyURI = 32;

    /** reg_address table */
    public static int $regAddressLineText1 = 4;
    public static int $regAddressLineText2 = 5;
    public static int $regAddressPostTown = 6;
    public static int $regAddressCounty = 7;
    public static int $regAddressCountry = 8;
    public static int $regAddressPostcode = 9;
    public static int $regAddressPobox = 3;
    public static int $regAddressCareOf = 2;

    /** account_category table */
    public static int $accountCategoryCategory = 19;
    public static int $accountCategoryRefDay = 15;
    public static int $accountCategoryRefMonth = 16;
    public static int $accountCategoryNextDueDate = 17;
    public static int $accountCategoryLastMadeUpdate = 18;

    /** mortgages table */
    public static int $mortgagesNumMortCharges = 22;
    public static int $mortgagesNumMortOutstanding = 23;
    public static int $mortgagesNumMortPartSatisfied = 24;
    public static int $mortgagesNumMortSatisfied = 25;

    /** CompanyPreviousName table */
    public static int $companyPreviousName_Name1 = 34;
    public static int $companyPreviousName_Condate1 = 33;
    public static int $companyPreviousName_Name2 = 36;
    public static int $companyPreviousName_Condate2 = 35;
    public static int $companyPreviousName_Name3 = 38;
    public static int $companyPreviousName_Condate3 = 37;
    public static int $companyPreviousName_Name4 = 40;
    public static int $companyPreviousName_Condate4 = 39;
    public static int $companyPreviousName_Name5 = 42;
    public static int $companyPreviousName_Condate5 = 41;
    public static int $companyPreviousName_Name6 = 44;
    public static int $companyPreviousName_Condate6 = 43;
    public static int $companyPreviousName_Name7 = 46;
    public static int $companyPreviousName_Condate7 = 45;
    public static int $companyPreviousName_Name8 = 48;
    public static int $companyPreviousName_Condate8 = 47;
    public static int $companyPreviousName_Name9 = 50;
    public static int $companyPreviousName_Condate9 = 49;
    public static int $companyPreviousName_Name10 = 52;
    public static int $companyPreviousName_Condate10 = 51;

    /** $limitedPartnershipsNumGenPartners */
    public static int $limitedPartnershipsNumGenPartners = 30;
    public static int $limitedPartnershipsNumLimPartners = 31;

    /** returns table */
    public static int $returnsNumDueDate = 20;
    public static int $returnsLastMadeUpdate = 21;

    /** siccode table */
    public static int $sicCodeText1 = 26;
    public static int $sicCodeText2 = 27;
    public static int $sicCodeText3 = 28;
    public static int $sicCodeText4 = 29;
}