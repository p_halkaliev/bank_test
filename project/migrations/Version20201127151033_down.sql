-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table project_db.accounts
DROP TABLE IF EXISTS `accounts`;
DROP TABLE IF EXISTS `account_category`;
DROP TABLE IF EXISTS `company`;
DROP TABLE IF EXISTS `company_category`;
DROP TABLE IF EXISTS `company_previous_name`;
DROP TABLE IF EXISTS `company_status`;
DROP TABLE IF EXISTS `country`;
DROP TABLE IF EXISTS `limited_partnerships`;
DROP TABLE IF EXISTS `mortgages`;
DROP TABLE IF EXISTS `reg_address`;
DROP TABLE IF EXISTS `returns`;
DROP TABLE IF EXISTS `sic_code`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
