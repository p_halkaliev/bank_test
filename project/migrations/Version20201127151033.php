<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201127151033 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(file_get_contents(__DIR__ . '/Version20201127151033_up.sql'));
    }

    public function down(Schema $schema) : void
    {
        $this->addSql(file_get_contents(__DIR__ . '/Version20201127151033_down.sql'));
    }
}
