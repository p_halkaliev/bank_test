window.onload = function (){
    const vueJsDelimiters = ['{-', '-}'];

    const resultComponent = Vue.component('result', {
        props: ["list","searched", "searching", "name"],
        delimiters: vueJsDelimiters,
        template: document.getElementById("result-template").innerText,
    });

   new Vue({
      el: '#app',
      delimiters: vueJsDelimiters,
      components: {
          resultComponent
      },
      data: {
         searched: false,
         searching: false,
         list:[],
         name: ""
      },
       methods:{
           search:function(event){
               this.searched = true;
               this.searching = true;
               this.list = [];

               const params = new URLSearchParams();
               params.append('name', this.name);

               const othis = this;
               axios.post('/search', params)
                   .then(function (response) {
                       console.log("response.data.result",response.data.result);
                       othis.list = response.data.hasOwnProperty("result") ? response.data.result : [];

                       othis.searching = false;
                   })
                   .catch(function (error) {
                       this.list = [];
                       othis.searching = false;
                       alert("Ajax error occur");
                       console.log("error: ", error );
                   });
           }
       },
   });
}